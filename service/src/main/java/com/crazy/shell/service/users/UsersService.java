package com.crazy.shell.service.users;

import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.bean.users.UserDetail;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface UsersService extends BaseService<Users> {

    /**
     * 根据users统计数量
     * @param users
     * @return
     */
    public int countOrObj(Users users);

    /**
     * 事务处理
     * 人员注册生成company 和 users
     * @param company
     * @param users
     * @return
     */
    public ActionResult addTran(Company company,Users users);

    /**
     * 获取当前登录人信息
     * @param uid user的主键Id
     * @return
     */
    public UserDetail getLoginUserInfo (Integer uid);

    /**
     * 从catch 获取
     * @param uid
     * @return
     */
    public Users getFromCache(Integer uid);
}
