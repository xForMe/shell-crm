package com.crazy.shell.service.company;

import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Guest;
import com.crazy.shell.dao.entity.Users;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface CompanyService extends BaseService<Company> {

    /**
     * 根据company统计数量
     * @param company
     * @return
     */
    public int countByObj(Company company);


    /**
     * 从catch 获取
     * @param uid
     * @return
     */
    public Company getFromCache(Integer uid);

}
