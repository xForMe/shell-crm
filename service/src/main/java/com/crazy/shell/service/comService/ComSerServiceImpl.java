package com.crazy.shell.service.comService;

import com.crazy.shell.common.persistent.dao.mapper.Mapper;
import com.crazy.shell.common.persistent.service.impl.BaseServiceImpl;
import com.crazy.shell.dao.entity.ComService;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.mapper.mybatis.ComServiceMapper;
import com.crazy.shell.dao.mapper.mybatis.CompanyMapper;
import com.crazy.shell.service.company.CompanyService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
@Component
public class ComSerServiceImpl extends BaseServiceImpl<ComService> implements ComSerService {

    @Resource
    private ComServiceMapper comServiceMapper;

    @Override
    public Mapper<ComService> getMapper() {
        return comServiceMapper;
    }
}
