package com.crazy.shell.api.base;

import com.crazy.shell.common.persistent.dao.entity.BaseEntity;
import com.crazy.shell.common.result.ActionResult;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface BaseProvider<T extends BaseEntity> {
    /**
     * 新增T
     * @param t
     * @return
     */
    public ActionResult add(T t);

    /**
     * 主键更新
     * @param t
     * @return
     */
    public ActionResult update(T t);

    /**
     * 主键查询
     * @param id
     * @return
     */
    public ActionResult getById(Integer id);

    /**
     * 逻辑删除数据
     * @param id
     * @return
     */
    public ActionResult deleteById(Integer id);

}
