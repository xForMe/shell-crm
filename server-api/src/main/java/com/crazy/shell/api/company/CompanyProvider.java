package com.crazy.shell.api.company;

import com.crazy.shell.api.base.BaseProvider;
import com.crazy.shell.dao.entity.Company;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface CompanyProvider extends BaseProvider<Company> {
}
