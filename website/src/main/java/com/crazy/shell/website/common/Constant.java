package com.crazy.shell.website.common;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/1/20.
 */

public interface Constant {

    public String USER_SESSION="user_session";

    public int USER_SESSION_TIME = 3 * 24 * 60 *60;

    public String VCODE_SESSIONID = "vcode_session";
}
