package com.crazy.shell.website.controller.index;

import org.dom4j.rule.Mode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/2/15.
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping("index.htm")
    public String toIndex(ModelMap model){
//        model.put("layout", "/layout/empty.vm");
        return "/index/index";
    }
}
