package com.crazy.shell.website.controller.users;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by xForMe on 2016/1/19.
 */
@Controller
@RequestMapping("/user")
public class UsersController {

    @RequestMapping("toAdd.htm")
    public String toAdd(){

        return "/WEB-INF/view/user/add.vm";
    }
}
