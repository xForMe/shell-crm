package com.crazy.shell.website.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/21.
 */
public class RequestUtil {

    /**
     * 获取IP地址
     * @param request
     * @return
     */
    public static String getIpAddr(HttpServletRequest request){
        String ip = request.getHeader("x-forwarded-for");
        if(StringUtils.isEmpty(ip) || StringUtils.equalsIgnoreCase("unknown",ip)){
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(StringUtils.isEmpty(ip) || StringUtils.equalsIgnoreCase("unknown",ip)){
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(StringUtils.isEmpty(ip) || StringUtils.equalsIgnoreCase("unknown",ip)){
            ip = request.getRemoteAddr();
        }
        return ip;
    }



}
