package comn.crazy.shell.web.guest;

import com.alibaba.dubbo.config.annotation.Reference;
import com.crazy.shell.api.guest.GuestProvider;
import com.crazy.shell.dao.entity.Guest;
import comn.crazy.shell.web.AbstractUnitTest;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/3/4.
 */

public class GuestClientTest extends AbstractUnitTest {

    @Reference(version = "1.0.0")
    private GuestProvider guestProvider;
    @Test
    public void selectById(){
        Guest guestBO = guestProvider.selectById(1);
        System.out.println(guestBO);
    }

    @Test
    public void findByObj(){
        Guest config = new Guest();
        config.setId(1);
        List<Guest> list = guestProvider.findByObj(config);
        System.out.println(list);
    }
}
