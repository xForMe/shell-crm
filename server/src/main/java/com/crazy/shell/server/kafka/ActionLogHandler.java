package com.crazy.shell.server.kafka;

import com.alibaba.fastjson.JSONObject;
import com.crazy.shell.dao.entity.repository.ActionLog;
import com.crazy.shell.kafka.handler.MessageHandler;
import com.crazy.shell.service.actionLog.ActionLogService;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/21.
 */
public class ActionLogHandler implements MessageHandler {

    @Resource
    private ActionLogService actionLogService;
    @Override
    public void handler(String s, byte[] bytes) {
        if(StringUtils.equals(s,"action_log")){
            String logStr = new String(bytes);
            ActionLog log = JSONObject.parseObject(logStr,ActionLog.class);
            actionLogService.save(log);
        }
    }
}
