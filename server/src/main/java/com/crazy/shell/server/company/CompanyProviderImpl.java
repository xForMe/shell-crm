package com.crazy.shell.server.company;

import com.crazy.shell.api.base.BaseProvider;
import com.crazy.shell.api.company.CompanyProvider;
import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.common.persistent.service.impl.BaseServiceImpl;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.server.base.BaseProviderImpl;
import com.crazy.shell.service.company.CompanyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
@com.alibaba.dubbo.config.annotation.Service
public class CompanyProviderImpl extends BaseProviderImpl<Company> implements CompanyProvider {

    @Resource
    private CompanyService companyService;

    @Override
    public BaseService<Company> getBaseService() {
        return companyService;
    }



    

}
