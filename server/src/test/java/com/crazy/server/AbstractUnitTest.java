package com.crazy.server;


import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/server-context.xml"})
public abstract class AbstractUnitTest {
	static {

		System.setProperty("resources.config.path", "/Users/creazier.huang/Desktop/config/shell");

	}

}
