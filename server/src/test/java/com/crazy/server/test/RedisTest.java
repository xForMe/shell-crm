package com.crazy.server.test;

import com.crazy.server.AbstractUnitTest;
import com.crazy.shell.common.redis.RedisClientTemplate;
import org.junit.Test;

import javax.annotation.Resource;
import javax.swing.*;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/8/4.
 */
public class RedisTest extends AbstractUnitTest {


    @Resource
    private RedisClientTemplate redisTemp;


    @Test
    public void test(){
        redisTemp.set("temp","test");
    }

    @Test
    public void get(){
        System.out.println(redisTemp.get("temp"));
    }

}
