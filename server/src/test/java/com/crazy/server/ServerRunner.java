package com.crazy.server;

import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class ServerRunner {


	public static void main(String[] args) throws Exception {

		System.setProperty("resources.config.path", "/Users/creazier.huang/Desktop/config/shell");

		Tomcat tomcat = new Tomcat();
		String baseDir = new File("").getAbsolutePath();

		// tomcat home自己新建一个，里面就只建一个webapps空目录
		// 数据源配在context.xml里，可以从普通tomcat里拷贝过来
		// 放一个默认的web.xml,从普通tomcat里拷贝过来
		tomcat.setBaseDir(baseDir + "/src/test/resources/tomcat-home");
		tomcat.enableNaming();
		tomcat.addWebapp("/crm-server", baseDir + "/server/src/main/webapp");
		// http22
		tomcat.setPort(8082);
		tomcat.start();
		tomcat.getServer().await();
	}
	//

}
