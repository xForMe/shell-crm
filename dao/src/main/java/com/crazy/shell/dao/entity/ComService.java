package com.crazy.shell.dao.entity;

import com.crazy.shell.common.persistent.dao.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "shell_service")
public class ComService extends BaseEntity implements Serializable {


    @Column(name = "COMPANY_ID")
    private Integer companyId;

    @Column(name = "NAME")
    private String name;




    private static final long serialVersionUID = 1L;



    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }


    @Override
    public String toString() {
        return "ComService{" +
                "companyId=" + companyId +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }
}