package com.crazy.shell.dao.repository;

import com.crazy.shell.dao.entity.repository.ActionLog;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/19.
 */
public interface ActionLogRepository extends MongoRepository<ActionLog,Long> {


}
