package com.crazy.shell.dao.mapper.mybatis;

import com.crazy.shell.common.persistent.dao.mapper.Mapper;
import com.crazy.shell.dao.entity.ComService;

public interface ComServiceMapper extends Mapper<ComService> {

}